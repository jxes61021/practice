module.exports = {
    'root': true,
    'env': {
        'node': true,
    },
    'extends': [
        'plugin:vue/vue3-essential',
        '@vue/standard'
    ],
    'parserOptions': {
        'parser': 'babel-eslint',
    },


    // // required to lint *.vue files
    // 'plugins': [
    //   'vue',
    // ],

    // add your custom rules here
    'rules': {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        // 'generator-star-spacing': 'off',
        // 'vue/order-in-components': [
        //   'error',
        //   {
        //     'order': [
        //       'el',
        //       'name',
        //       'parent',
        //       'functional',
        //       [
        //         'delimiters',
        //         'comments',
        //       ],
        //       [
        //         'components',
        //         'directives',
        //         'filters',
        //       ],
        //       'extends',
        //       'mixins',
        //       'inheritAttrs',
        //       'model',
        //       [
        //         'props',
        //         'propsData',
        //       ],
        //       'data',
        //       'computed',
        //       'watch',
        //       'LIFECYCLE_HOOKS',
        //       'methods',
        //       [
        //         'template',
        //         'render',
        //       ],
        //       'renderError',
        //     ],
        //   },
        // ],
        'comma-dangle': [
            'error',
            'always-multiline',
        ],
        'radix': ['error', 'as-needed'],
        'no-var': 'error',
        'no-unused-vars': 'error',
        // 'no-extend-native': 'off',
        'prefer-promise-reject-errors': 'off',
        // 'standard/no-callback-literal': 'off',
        'line-comment-position': [
            'error', {
                'position': 'above',
            },
        ],
    },
}