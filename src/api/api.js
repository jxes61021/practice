import axios from 'axios'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

const api = axios.create({ baseURL: 'http://ma-gpg.ceis.tw/' })
let apitoken = localStorage.getItem('token') || null
const tokenApi = async (token) => {
  let data = null
  localStorage.setItem('token', token)
  apitoken = token
  await axios({
    method: 'GET',
    url: 'http://ma-gpg.ceis.tw/Token/Menu',
    responseType: 'json',
    headers: { Authorization: 'Bearer ' + apitoken },
  }).then(function (response) {
    // console.log(response)
    data = response
  })
    .catch(function (error) {
      console.log('錯誤', error)
    })
  return data
}

const WalletChange = async (params) => {
  let data = null
  const baseurl = 'http://ma-gpg.ceis.tw/DataReport/WalletChangeReport'
  const urls = baseurl + params
  await axios({
    method: 'GET',
    url: urls,
    responseType: 'json',
    headers: { Authorization: 'Bearer ' + apitoken },
  }).then(function (response) {
    // console.log(response.data)
    data = response
  })
    .catch(function (error) {
      console.log('錯誤', error)
    })
  return data
}
export const loginApi = data => api.post('Token/SignIn', data)
export { tokenApi, WalletChange }
