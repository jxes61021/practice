import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '../src/assets/custom-theme/index.css'

const app = createApp(App)
app.use(ElementPlus)
app.use(store).use(router).mount('#app')
