import { createRouter, createWebHashHistory } from 'vue-router'
import login from '../views/login.vue'
import backoffice from '../views/backoffice.vue'
import changepoint from '../views/changepoint.vue'
import register from '../views/register.vue'
const routes = [{
  path: '/',
  name: 'login',
  component: login,
},
{
  path: '/backoffice',
  name: 'backoffice',
  component: backoffice,
  children: [
    {
      path: '/changepoint',
      name: 'changepoint',
      component: changepoint,
    },
  ],
},
{
  path: '/register',
  name: 'register',
  component: register,
},
{
  path: '/:catchAll(.*)',
  redirect: '/',
},
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
