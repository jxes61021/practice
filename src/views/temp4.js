// 禮包內容
export const REWARD_COIN = [
  {
    id: 0,
    label: 'GoldPoint',
    img: './img/coin/i_money_gold.svg',
    background: 'linear-gradient(180deg ,#000 0,#3d3d3d)',
    border: '1px solid #8940a7',
    text: '#fff',
  }, {
    id: 1,
    label: 'SilverPoint',
    img: './img/coin/i_money_silver.svg',
    background: 'linear-gradient(180deg ,#000 0,#3d3d3d)',
    border: '1px solid #8940a7',
    text: '#fff',
  }, {
    id: 2,
    label: 'Exp',
    img: './img/coin/i_vip_color.svg',
    background: 'linear-gradient(180deg, #fff 0%, #B7B3AF 75%, #fff 100%)',
    // background: 'linear-gradient(180deg,#fff 0,#b7b7b7 90%,#fff)',
    text: '#dc0000',
    point: '+',
  }, {
    id: 3,
    label: 'DragonBoat',
    img: './img/coin/i_vip_color.svg',
    point: 'x',
  },
]

export const REWARD_BACKGROUND = {
  // 預設
  0: './img/deposit/background/deposit_deposit_bg01.jpg',

  // 儲值禮包
  // 禮包3, 4
  10131: './img/deposit/background/store_normal01.jpg',
  10132: './img/deposit/background/store_normal01.jpg',
  10133: './img/deposit/background/store_normal01.jpg',
  10134: './img/deposit/background/store_normal01.jpg',
  10135: './img/deposit/background/store_normal01.jpg',
  10136: './img/deposit/background/store_normal01.jpg',
  // 禮包1, 2
  10125: './img/deposit/background/store_normal02.jpg',
  10126: './img/deposit/background/store_normal02.jpg',
  10127: './img/deposit/background/store_normal02.jpg',
  10128: './img/deposit/background/store_normal02.jpg',
  10129: './img/deposit/background/store_normal02.jpg',
  10130: './img/deposit/background/store_normal02.jpg',

  // 日禮包
  // new
  10137: './img/deposit/background/store_normal_d.jpg',
  10138: './img/deposit/background/store_normal_d.jpg',
  10139: './img/deposit/background/store_normal_d.jpg',

  // 週禮包
  // new
  10140: './img/deposit/background/store_normal_w.jpg',
  10141: './img/deposit/background/store_normal_w.jpg',
  10142: './img/deposit/background/store_normal_w.jpg',

  // 月禮包
  // new
  10143: './img/deposit/background/store_normal_m.jpg',
  10144: './img/deposit/background/store_normal_m.jpg',
  10145: './img/deposit/background/store_normal_m.jpg',

  // 1月禮包
  10158: './img/deposit/background/store_limit202201_0.jpg',
  10155: './img/deposit/background/store_limit202201_4.jpg',
  10156: './img/deposit/background/store_limit202201_4.jpg',
  10157: './img/deposit/background/store_limit202201_4.jpg',
  10152: './img/deposit/background/store_limit202201_3.jpg',
  10153: './img/deposit/background/store_limit202201_3.jpg',
  10154: './img/deposit/background/store_limit202201_3.jpg',
  10149: './img/deposit/background/store_limit202201_2.jpg',
  10150: './img/deposit/background/store_limit202201_2.jpg',
  10151: './img/deposit/background/store_limit202201_2.jpg',
  10146: './img/deposit/background/store_limit202201_1.jpg',
  10147: './img/deposit/background/store_limit202201_1.jpg',
  10148: './img/deposit/background/store_limit202201_1.jpg',
}

export const SALE_IMG = {
  10155: '/img/deposit/sale/store_imgsale300p.png',
  10156: '/img/deposit/sale/store_imgsale300p.png',
  10157: '/img/deposit/sale/store_imgsale300p.png',
  10152: '/img/deposit/sale/store_imgsale300p.png',
  10153: '/img/deposit/sale/store_imgsale300p.png',
  10154: '/img/deposit/sale/store_imgsale300p.png',
  10149: '/img/deposit/sale/store_imgsale300p.png',
  10150: '/img/deposit/sale/store_imgsale300p.png',
  10151: '/img/deposit/sale/store_imgsale300p.png',
  10146: '/img/deposit/sale/store_imgsale1000p.png',
  10147: '/img/deposit/sale/store_imgsale1000p.png',
  10148: '/img/deposit/sale/store_imgsale1000p.png',
}

export const DEPOSIT_TYPE = [
  { id: 2, type: 'card', label: '信用卡' },
  { id: 1, type: 'atm', label: 'ATM轉帳' },
  { id: 3, type: 'code', label: '超商代碼繳費' },
]
