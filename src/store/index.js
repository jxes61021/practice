import { createStore } from 'vuex'

export default createStore({
  state: {
    patharray: [
      ['廣告加盟商管理'],
      ['會員管理', '會員列表', '補/扣點紀錄查詢'],
      ['活動管理'],
      ['遊戲管理'],
      ['報表管理', '會員點數異動紀錄', '會員儲值紀錄', '儲值報表', '會員交易紀錄', '會員礦機領取紀錄', '礦機紀錄'],
    ],
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  },
})
